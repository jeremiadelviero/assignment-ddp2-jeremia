import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import javari.animal.Animal;
import javari.park.Attraction;
import javari.park.Registration;
import javari.park.SelectedAttraction;
import javari.park.Visitor;
import javari.reader.AttractionsReader;
import javari.reader.CategoriesReader;
import javari.reader.CsvReader;
import javari.reader.RecordsReader;
import javari.writer.RegistrationWriter;

public class A3Festival {
//  Scanner untuk meminta input
    private static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("Welcome to Javari Park Festival - Registration Service!\n");
        System.out.print("... Opening default section database from data. ... ");

        CsvReader[] fileReaders = new CsvReader[3];

        String filesPath = System.getProperty("user.dir")
                + "\\assignment-3\\src\\main\\java\\javari";
        System.out.println(System.getProperty("user.dir"));

//      Meminta path yang berisi 3 file yang dibutuhkan
        while (true) {
            try {
                fileReaders[0] = new CategoriesReader(Paths.get(filesPath,
                        "animals_categories.csv"));
                fileReaders[1] = new AttractionsReader(Paths.get(filesPath,
                        "animals_attractions.csv"));
                fileReaders[2] = new RecordsReader(Paths.get(filesPath,
                        "animals_records.csv"));
                System.out.println("\n... Loading... Success... System is populating data...\n");
                break;
            } catch (IOException e) {
                System.out.println("... File not found or incorrect file!\n");
                System.out.print("Please provide the source data path: ");
                filesPath = sc.nextLine().replace("\\", "\\\\");
            }
        }

        System.out.printf("Found %d valid sections and %d invalid sections\n",
                fileReaders[0].countValidRecords(), fileReaders[0].countInvalidRecords());
        System.out.printf("Found %d valid attractions and %d invalid attractions\n",
                fileReaders[1].countValidRecords(), fileReaders[1].countInvalidRecords());
        System.out.printf("Found %d valid animal categories and %d invalid animal categories\n",
                fileReaders[0].countValidRecords(), fileReaders[0].countInvalidRecords());
        System.out.printf("Found %d valid animal records and %d invalid animal records\n",
                fileReaders[2].countValidRecords(), fileReaders[2].countInvalidRecords());

        registrationService();

    }
//  Method yang mengeluarkan output meminta section yang paling awal
    public static void registrationService() {
        System.out.println("Welcome to Javari Park Festival - Registration Service!\n");
        System.out.println("Please answer the questions by typing the number. "
                + "Type # if you want to return to the previous menu");

        sectionsInput();
    }

//  Meminta input dari pilihan section yang ada
    public static void sectionsInput() {
        String input;

        while (true) {
            ArrayList<String> sectionAvailable = CategoriesReader.getSections();
            System.out.println("\nJavari Park has " + sectionAvailable.size() + " sections:");

            int i = 0;
            for (String section : sectionAvailable) {
                System.out.println(++i + ". " + section);
            }

            System.out.print("Please choose your preferred section (type the number): ");

            input = sc.nextLine();
            if (input.equals("#")) {
                registrationService();
                return;
            }

            try {
                int inputInt = Integer.parseInt(input);
                if (inputInt > 0 && inputInt <= i) {
                    animalCheck(sectionAvailable.get(inputInt - 1));
                    return;
                } else {
                    System.out.println("Wrong input");
                }
            } catch (NumberFormatException e) {
                System.out.println("Wrong input");
            }
        }
    }

//  Meminta input dari pilihan animals yang ada pada section yang telah dipilih
    public static void animalCheck(String section) {
        String input;

        while (true) {
            System.out.println("\n--" + section + "--");
            int i = 0;
            List<String> animalsDefault = CsvReader.createAnimalsMap().get(section);
            List<String> animalsAvailable = new ArrayList<>();
            for (String animal : animalsDefault) {
                if (CategoriesReader.getAnimals().contains(animal)) {
                    animalsAvailable.add(animal);
                }
            }
            for (String animal : animalsAvailable) {
                System.out.println(++i + ". " + animal);
            }

            System.out.print("Please choose your preferred animals (type the number): ");

            input = sc.nextLine();
            if (input.equals("#")) {
                sectionsInput();
                return;
            }

            try {
                int inputInt = Integer.parseInt(input);
                if (inputInt > 0 && inputInt <= i) {
                    attractionCheck(animalsAvailable.get(inputInt - 1), section);
                    return;
                } else {
                    System.out.println("Wrong input");
                }
            } catch (NumberFormatException e) {
                System.out.println("Wrong input");
            }
        }
    }

    //  Meminta input dari pilihan attraction yang ada dari animal yang dipilih
    public static void attractionCheck(String animal, String section) {
        String input;

        ArrayList<SelectedAttraction> attractions = Attraction.findAttractions(animal);

        boolean isEmpty = true;

        for (SelectedAttraction attraction : attractions) {
            if (!attraction.getPerformers().isEmpty()) {
                isEmpty = false;
                break;
            }
        }

        if (isEmpty) {
            System.out.println("\nUnfortunately, no " + animal
                    + " can perform any attraction, please choose other animals");
            animalCheck(section);
            return;
        }

        while (true) {
            System.out.println("\n--" + animal + "--");
            int i = 0;
            ArrayList<String> temp = new ArrayList<>();
            for (SelectedAttraction attraction : attractions) {
                System.out.println(++i + ". " + attraction.getName());
                temp.add(attraction.getName());
            }

            System.out.print("Please choose your preferred attractions (type the number): ");

            input = sc.nextLine();
            if (input.equals("#")) {
                animalCheck(section);
                return;
            }

            try {
                int inputInt = Integer.parseInt(input);
                if (inputInt > 0 && inputInt <= i) {
                    finalCheck(attractions.get(inputInt - 1));
                    return;
                } else {
                    System.out.println("Wrong input");
                }
            } catch (NumberFormatException e) {
                System.out.println("Wrong input");
            }
        }
    }

    //  Meminta input untuk memasukkan data dari pengunjung / visitor
    public static void finalCheck(SelectedAttraction attraction) {
        String input;
        System.out.println("\nWow, one more step,");
        System.out.print("please let us know your name: ");
        String name = sc.nextLine();

        Registration visitor = Visitor.findVisitor(name);

        if (visitor == null) {
            visitor = new Visitor(name);
        }

        while (true) {
            System.out.println("\nYeay, final check!");
            System.out.println("Here is your data, and the attraction you chose:");
            System.out.println("Name: " + visitor.getVisitorName());
            System.out.println("Attractions: " + attraction.getName()
                    + " -> " + attraction.getType());
            System.out.print("With:");

            String output = "";

            for (Animal animal : attraction.getPerformers()) {
                output += " " + animal.getName() + ",";
            }

            System.out.println(output.substring(0, output.length() - 1));


            System.out.print("\nIs the data correct? (Y/N): ");

            input = sc.nextLine();
            if (input.equalsIgnoreCase("Y")) {
                visitor.addSelectedAttraction(attraction);
                System.out.print("\nThank you for your interest."
                        + " Would you like to register to other attractions? (Y/N): ");
                String input2 = sc.nextLine();
                if (input2.equalsIgnoreCase("Y")) {
                    registrationService();
                    return;
                } else if (input2.equalsIgnoreCase("N")) {
                    String filePath = System.getProperty("user.dir")
                            + "\\assignment-3\\src\\main\\java\\javari";
                    for (Registration visitorRn : Visitor.getVisitors()) {
                        try {
                            RegistrationWriter.writeJson(visitorRn, Paths.get(filePath));
                        } catch (IOException e) {
                            System.out.println("Something went wrong with "
                                    + visitorRn.getVisitorName());
                        }
                    }
                    return;
                } else {
                    System.out.println("Wrong input");
                }
            } else if (input.equalsIgnoreCase("N")) {
                System.out.print("Ganti Nama: ");
                visitor.setVisitorName(sc.nextLine());
            } else {
                System.out.println("Wrong input");
            }
        }
    }
}
