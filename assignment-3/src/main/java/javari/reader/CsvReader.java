package javari.reader;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * This class represents the base class for reading all lines from a text
 * file that contains CSV data.
 *
 * @author Programming Foundations 2 Teaching Team
 * @author Jeremia Delviero Hutagalung
 *     and describe the changes in the comment block
 */
public abstract class CsvReader {

    public static final String COMMA = ",";

    private final Path file;
    private final List<String> lines;

    protected static final Map<String, List<String>> categoriesMap = createCategoriesMap();
    protected static final Map<String, List<String>> attractionsMap = createAttractionsMap();
    protected static final Map<String, List<String>> animalsMap = createAnimalsMap();

    /**
     * Defines the base constructor for instantiating an object of
     * {@code CsvReader}.
     *
     * @param file  path object referring to a CSV file
     * @throws IOException if given file is not present or cannot be read
     *     properly
     */
    public CsvReader(Path file) throws IOException {
        this.file = file;
        this.lines = Files.readAllLines(this.file, StandardCharsets.UTF_8);
    }

    public static Map<String, List<String>> createCategoriesMap() {
        Map<String, List<String>> map = new HashMap<>();
        map.put("mammals", new ArrayList<>(Arrays.asList("Hamster", "Lion", "Cat", "Whale")));
        map.put("aves", new ArrayList<>(Arrays.asList("Eagle", "Parrot")));
        map.put("reptiles", new ArrayList<>(Arrays.asList("Snake")));
        return map;
    }

    public static Map<String, List<String>> createAttractionsMap() {
        Map<String, List<String>> map = new HashMap<>();
        map.put("Circles of Fires", new ArrayList<>(Arrays.asList("Lion", "Eagle", "Whale")));
        map.put("Dancing Animals", new ArrayList<>(Arrays.asList("Snake", "Parrot",
                "Cat", "Hamster")));
        map.put("Counting Masters", new ArrayList<>(Arrays.asList("Hamster", "Whale", "Parrot")));
        map.put("Passionate Coders", new ArrayList<>(Arrays.asList("Hamster", "Cat", "Snake")));
        return map;
    }

    public static Map<String, List<String>> createAnimalsMap() {
        Map<String, List<String>> map = new HashMap<>();
        map.put("Explore the Mammals", new ArrayList<>(Arrays.asList("Hamster", "Lion",
                "Cat", "Whale")));
        map.put("World of Aves", new ArrayList<>(Arrays.asList("Eagle", "Parrot")));
        map.put("Reptillian Kingdom", new ArrayList<>(Arrays.asList("Snake")));
        return map;
    }

    public static Map<String, List<String>> getAnimalsMap() {
        return animalsMap;
    }



    /**
     * Returns all line of text from CSV file as a list.
     *
     * @return
     */
    public List<String> getLines() {
        return lines;
    }

    /**
     * Counts the number of valid records from read CSV file.
     *
     * @return
     */
    public abstract long countValidRecords();

    /**
     * Counts the number of invalid records from read CSV file.
     *
     * @return
     */
    public abstract long countInvalidRecords();
}
