package javari.reader;

import java.io.IOException;
import java.nio.file.Path;

import javari.animal.Animal;
import javari.animal.Aves;
import javari.animal.Mammals;
import javari.animal.Reptiles;
import javari.park.Attraction;


public class RecordsReader extends CsvReader {

    private static long validRecords;
    private static long totalRecords;

    public RecordsReader(Path file) throws IOException {
        super(file);
    }

//  Menghitung jumlah record yang valid sekaligus memasukkan input file kedalam class Attraction
    public long countValidRecords() {
        validRecords = 0;
        totalRecords = 0;

        Attraction cofLion = new Attraction("Circle of Fire", "Lion");
        Attraction cofWhale = new Attraction("Circle of Fire", "Whale");
        Attraction cofEagle = new Attraction("Circle of Fire", "Eagle");
        Attraction daParrot = new Attraction("Dancing Animals", "Parrot");
        Attraction daSnake = new Attraction("Dancing Animals", "Snake");
        Attraction daCat = new Attraction("Dancing Animals", "Cat");
        Attraction daHamster = new Attraction("Dancing Animals", "Hamster");
        Attraction cmHamster = new Attraction("Counting Masters", "Hamster");
        Attraction cmWhale = new Attraction("Counting Masters", "Whale");
        Attraction cmParrot = new Attraction("Counting Masters", "Parrot");
        Attraction pcHamster = new Attraction("Passionate Coders", "Hamster");
        Attraction pcCat = new Attraction("Passionate Coders", "Cat");
        Attraction pcSnake = new Attraction("Passionate Coders", "Snake");
        for (String line : getLines()) {
            String[] info = line.split(COMMA);
            Animal temp;
            totalRecords++;


            if (categoriesMap.get("mammals").contains(info[1])) {
                temp = new Mammals(info);
            } else if (categoriesMap.get("aves").contains(info[1])) {
                temp = new Aves(info);
            } else if (categoriesMap.get("reptiles").contains(info[1])) {
                temp = new Reptiles(info);
            } else {
                temp = null;
            }
            if (attractionsMap.get("Circles of Fires").contains(info[1])) {
                switch (info[1]) {
                    case "Lion":
                        cofLion.addPerformer(temp);
                        break;
                    case "Whale":
                        cofWhale.addPerformer(temp);
                        break;
                    case "Eagle":
                        cofEagle.addPerformer(temp);
                        break;
                    default:
                        break;
                }
            }
            if (attractionsMap.get("Dancing Animals").contains(info[1])) {
                switch (info[1]) {
                    case "Parrot":
                        daParrot.addPerformer(temp);
                        break;
                    case "Snake":
                        daSnake.addPerformer(temp);
                        break;
                    case "Cat":
                        daCat.addPerformer(temp);
                        break;
                    case "Hamster":
                        daHamster.addPerformer(temp);
                        break;
                    default:
                        break;
                }
            }
            if (attractionsMap.get("Counting Masters").contains(info[1])) {
                switch (info[1]) {
                    case "Hamster":
                        cmHamster.addPerformer(temp);
                        break;
                    case "Whale":
                        cmWhale.addPerformer(temp);
                        break;
                    case "Parrot":
                        cmParrot.addPerformer(temp);
                        break;
                    default:
                        break;
                }
            }
            if (attractionsMap.get("Passionate Coders").contains(info[1])) {
                switch (info[1]) {
                    case "Hamster":
                        pcHamster.addPerformer(temp);
                        break;
                    case "Cat":
                        pcCat.addPerformer(temp);
                        break;
                    case "Snake":
                        pcSnake.addPerformer(temp);
                        break;
                    default:
                        break;
                }
            }
            if (temp != null) {
                validRecords++;
            }
        }

        return validRecords;
    }

    public long countInvalidRecords() {
        return totalRecords - validRecords;
    }

}
