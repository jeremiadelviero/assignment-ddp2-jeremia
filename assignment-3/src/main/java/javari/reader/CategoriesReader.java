package javari.reader;

import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class CategoriesReader extends CsvReader {

    private static ArrayList<String> sections;
    private static ArrayList<String> animals = new ArrayList<>();

    public CategoriesReader(Path file) throws IOException {
        super(file);
    }

    public static ArrayList<String> getSections() {
        return sections;
    }

    public static ArrayList<String> getAnimals() {
        return animals;
    }

    public long countValidRecords() {
        Set<String> validRecords = new HashSet<>();
        for (String line : getLines()) {
            String[] info = line.split(COMMA);
            if (categoriesMap.containsKey(info[1]) && animalsMap.containsKey(info[2])) {
                if (categoriesMap.get(info[1]).contains(info[0])) {
                    validRecords.add(info[2]);
                    animals.add(info[0]);
                }
            }
        }
        sections = new ArrayList<>(validRecords);
        return (long) validRecords.size();
    }

    public long countInvalidRecords() {
        long invalid = 0;
        for (String line : getLines()) {
            String[] info = line.split(COMMA);
            if (info.length == 3) {
                if ((!categoriesMap.containsKey(info[1])) || (!animalsMap.containsKey(info[2]))) {
                    invalid++;
                } else if (categoriesMap.containsKey(info[1])) {
                    if (!(categoriesMap.get(info[1]).contains(info[0]))) {
                        invalid++;
                    }
                }
            }
        }
        return invalid;
    }

}
