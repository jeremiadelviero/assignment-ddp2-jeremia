package javari.reader;

import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class AttractionsReader extends CsvReader {

    public AttractionsReader(Path file) throws IOException {
        super(file);
    }


    public long countValidRecords() {
        Set<String> validRecords = new HashSet<>();

        for (String line : getLines()) {
            String[] info = line.split(COMMA);
            if (info.length == 2) {
                if (attractionsMap.containsKey(info[1])) {
                    if (attractionsMap.get(info[1]).contains(info[0])
                            && CategoriesReader.getAnimals().contains(info[0])) {
                        validRecords.add(info[1]);
                    }
                }
            }
        }
        return (long) validRecords.size();
    }

    public long countInvalidRecords() {
        long invalid = 0;

        for (String line : getLines()) {
            String[] info = line.split(COMMA);
            if (info.length == 3) {
                if (!attractionsMap.containsKey(info[1])) {
                    invalid++;
                } else if (attractionsMap.containsKey(info[1])) {
                    if (!attractionsMap.get(info[1]).contains(info[0])
                            || !CategoriesReader.getAnimals().contains(info[0])) {
                        invalid++;
                    }
                }
            }
        }
        return invalid;
    }
}
