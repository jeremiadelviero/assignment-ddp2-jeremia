package javari.animal;

public class Mammals extends Animal {
    private String specificCondition;

    public Mammals(String[] info) {
        super(Integer.parseInt(info[0]), info[1], info[2], Gender.parseGender(info[3]),
                Double.parseDouble(info[4]), Double.parseDouble(info[5]),
                Condition.parseCondition(info[7]));
        this.specificCondition = info[6];
    }

//  Spesific Condition untuk Mammals dapat melakukan atraksi jika tidak hamil dan jika singa bukan perempuan
    public boolean specificCondition() {
        if (specificCondition.equalsIgnoreCase("pregnant")) {
            return false;
        } else if (getType().equalsIgnoreCase("Lion") && getGender() == Gender.FEMALE) {
            return false;
        }
        return true;
    }
}
