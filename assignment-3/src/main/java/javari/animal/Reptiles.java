package javari.animal;

public class Reptiles extends Animal {
    private String specificCondition;

    public Reptiles(String[] info) {
        super(Integer.parseInt(info[0]), info[1], info[2], Gender.parseGender(info[3]),
                Double.parseDouble(info[4]), Double.parseDouble(info[5]),
                Condition.parseCondition(info[7]));
        this.specificCondition = info[6];
    }

//  Spesific Condition untuk Reptiles dapat melakukan atraksi jika jinak
    public boolean specificCondition() {
        if (specificCondition.equalsIgnoreCase("tame")) {
            return true;
        }
        return false;
    }
}
