package javari.animal;

public class Aves extends Animal {
    private String specificCondition;

    public Aves(String[] info) {
        super(Integer.parseInt(info[0]), info[1], info[2], Gender.parseGender(info[3]),
                Double.parseDouble(info[4]), Double.parseDouble(info[5]),
                Condition.parseCondition(info[7]));
        this.specificCondition = info[6];
    }

//  Spesific Condition untuk Aves dapat melakukan atraksi jika tidak sedang bertelur
    public boolean specificCondition() {
        if (specificCondition.equalsIgnoreCase("laying eggs")) {
            return false;
        }
        return true;
    }
}
