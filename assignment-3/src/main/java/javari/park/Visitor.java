package javari.park;

import java.util.ArrayList;
import java.util.List;

public class Visitor implements Registration {

//  List yang berisi instance-instance Visitor yang dimasukkan ketika di construct
    private static ArrayList<Registration> visitors = new ArrayList<>();
    private static int registrationIdAll = 1;
    private int registrationId;
    private String name;
    private List<SelectedAttraction> attractions = new ArrayList<>();


    public Visitor(String name) {
        this.name = name;
        this.registrationId = registrationIdAll++;
        visitors.add(this);
    }

//  Mencari instance attractions yang sudah disimpan
    public static Registration findVisitor(String name) {
        for (Registration visitor : visitors) {
            if (visitor.getVisitorName().equalsIgnoreCase(name)) {
                return visitor;
            }
        }
        return null;
    }

    public static ArrayList<Registration> getVisitors() {
        return visitors;
    }

    @Override
    public int getRegistrationId() {
        return registrationId;
    }

    @Override
    public String getVisitorName() {
        return name;
    }

    @Override
    public String setVisitorName(String name) {
        this.name = name;
        return this.name;
    }

    @Override
    public List<SelectedAttraction> getSelectedAttractions() {
        return attractions;
    }

    @Override
    public boolean addSelectedAttraction(SelectedAttraction selected) {
        attractions.add(selected);
        return true;
    }
}
