package javari.park;

import java.util.ArrayList;
import java.util.List;

import javari.animal.Animal;

public class Attraction implements SelectedAttraction {
    private String name;
    private String type;

//  List yang berisi instance-instance Attraction yang dimasukkan ketika di construct
    private static List<SelectedAttraction> allAttractions = new ArrayList<>();
    private List<Animal> performers = new ArrayList<>();

    public Attraction(String name, String type) {
        this.name = name;
        this.type = type;
        allAttractions.add(this);
    }

//  Mencari instance attractions yang sudah disimpan
    public static ArrayList<SelectedAttraction> findAttractions(String type) {
        ArrayList<SelectedAttraction> array = new ArrayList<>();
        for (SelectedAttraction attraction : allAttractions) {
            if (attraction.getType().equals(type)) {
                array.add(attraction);
            }
        }
        return array;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getType() {
        return type;
    }

    @Override
    public List<Animal> getPerformers() {
        return performers;
    }

    @Override
    /**
     * Adds a new animal into the list of performers.
     *
     * @param performer an instance of animal
     * @return {@code true} if the animal is successfully added into list of
     *     performers, {@code false} otherwise
     */
    public boolean addPerformer(Animal performer) {
        if (performer != null) {
            performers.add(performer);
        }
        return true;
    }
}
