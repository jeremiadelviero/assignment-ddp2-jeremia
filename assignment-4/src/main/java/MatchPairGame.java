import board.MatchPairBoard;

import javax.swing.SwingUtilities;

/**
 * Class yang berisi main method untuk menjalankan GUI.
 */
public class MatchPairGame {
    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                new MatchPairBoard();  // Let the constructor do the job
            }
        });
    }
}
