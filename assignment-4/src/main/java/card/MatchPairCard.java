package card;

import java.awt.Dimension;
import javax.swing.ImageIcon;
import javax.swing.JButton;

/**
 * Class yang merepresentasikan kartu yang ada di Match Pair Memory Game.
 */
public class MatchPairCard {

    /**
     * Instance variable yang dimiliki masing - masing objek.
     */
    private int id;
    private ImageIcon iconBack;
    private ImageIcon iconFront;
    private boolean matched = false;
    private JButton button = new JButton();

    /**
     * Constructor dari objek class MatchPairCard.
     *
     * @param id        angka unik dari masing - masing kartu dengan gambar yang berbeda
     * @param iconBack  gambar bagian belakang dari kartu (sebelum dibuka)
     * @param iconFront gambar bagian depan dari kartu (setelah dibuka)
     */
    public MatchPairCard(int id, ImageIcon iconBack, ImageIcon iconFront) {
        this.id = id;
        this.iconBack = iconBack;
        this.iconFront = iconFront;
        this.button.setPreferredSize(new Dimension(100, 141));
        this.button.setIcon(iconBack);
    }

    /**
     * Getter untuk id.
     *
     * @return id dari objek
     */
    public int getId() {
        return id;
    }

    /**
     * Getter untuk matched.
     *
     * @return boolean true jika objek telah menemukan pasangannya
     *         false jika objek belum menemukan pasangannya
     */
    public boolean isMatched() {
        return matched;
    }

    /**
     * Getter untuk button.
     *
     * @return Button yang ada di objek
     */
    public JButton getButton() {
        return button;
    }

    /**
     * Setter untuk matched.
     *
     * @param matched boolean yang merubah kondisi matched
     */
    public void setMatched(boolean matched) {
        this.matched = matched;
    }

    /**
     * Method untuk menampilkan gambar kartu dengan
     * merubah Icon yang ada di button menjadi gambar
     * dari iconFront.
     */
    public void show() {
        button.setIcon(iconFront);
    }

    /**
     * Method untuk menyembunyikan gambar kartu dengan
     * merubah Icon yang ada di button menjadi gambar
     * dari iconBack.
     */
    public void hide() {
        button.setIcon(iconBack);
    }
}
