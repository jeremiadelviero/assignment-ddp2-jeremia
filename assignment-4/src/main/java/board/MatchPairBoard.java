package board;

import card.MatchPairCard;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Image;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.Timer;
import javax.swing.WindowConstants;

/**
 * Class yang merepresentasikan Match Pair Memory Game.
 */
public class MatchPairBoard {

    /**
     * Instance variable yang dimiliki masing - masing objek.
     */
    private static final int FIXEDPAIRS = 18;
    private ArrayList<MatchPairCard> cardList = new ArrayList<>();
    private MatchPairCard selectedCard1;
    private MatchPairCard selectedCard2;
    private JFrame frame;
    private JPanel topPanel;
    private JPanel bottomPanel;
    private JLabel resetLabel;
    private JLabel tryLabel;
    private Timer checkCardsTimer;
    private int resetAttempts = 0;
    private int tryAttempts = 0;

    /**
     * Constructor class yang sekaligus menginisiasi
     * GUI yang akan dijalankan.
     */
    public MatchPairBoard() {
        ArrayList<Integer> cardIntList = new ArrayList<>();

        for (int i = 0; i < FIXEDPAIRS; i++) {
            cardIntList.add(i);
            cardIntList.add(i);
        }
        Collections.shuffle(cardIntList);

        String path = System.getProperty("user.dir") + "\\src\\main\\img\\";
        try {
            for (int id : cardIntList) {
                MatchPairCard card = new MatchPairCard(id,
                        resizeImage(new ImageIcon(path + "pokemon.png")),
                        resizeImage(new ImageIcon(path + id + ".png")));
                card.getButton().addActionListener(act -> flipCard(card));
                cardList.add(card);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        frame = new JFrame("Remember the Pokémon");
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setLayout(new BorderLayout());
        topPanel = new JPanel(new GridLayout(6, 6));
        bottomPanel = new JPanel(new GridLayout(2, 2));
        resetLabel = new JLabel("Number of Reset: " + resetAttempts);
        tryLabel = new JLabel("Number of Reset: " + tryAttempts);
        for (MatchPairCard card : cardList) {
            topPanel.add(card.getButton());
        }
        JButton resetButton = new JButton("Reset Game");
        JButton exitButton = new JButton("Exit");
        bottomPanel.add(resetButton);
        bottomPanel.add(exitButton);
        bottomPanel.add(resetLabel);
        bottomPanel.add(tryLabel);
        resetButton.addActionListener(act -> resetGame(cardList));
        exitButton.addActionListener(act -> System.exit(0));

        frame.add(topPanel, BorderLayout.CENTER);
        frame.add(bottomPanel, BorderLayout.SOUTH);
        frame.setPreferredSize(new Dimension(630, 960));
        frame.setResizable(false);
        frame.setLocation(600, 50);
        frame.pack();
        frame.setVisible(true);

        checkCardsTimer = new Timer(750, act -> checkCards());
        checkCardsTimer.setRepeats(false);

        Timer showAllTimer = new Timer(750, act -> showAll());
        showAllTimer.setRepeats(false);
        showAllTimer.start();
    }

    /**
     * Method untuk menyesuaikan ukuran Image agar pas dengan ukuran Button.
     *
     * @param imageIcon ImageIcon sebelum di resize
     * @return ImageIcon yang sudah di resize
     */
    public static ImageIcon resizeImage(ImageIcon imageIcon) {
        Image image = imageIcon.getImage();
        Image resizedImage = image.getScaledInstance(100, 141, java.awt.Image.SCALE_SMOOTH);
        return new ImageIcon(resizedImage);
    }

    /**
     * Method untuk membuka 2 kartu yang diklik dan mengecek apakah
     * kedua kartu tersebut sama atau tidak dengan memanggil fungsi checkCards().
     *
     * @param selectedCard MatchPairCard / kartu yang telah di klik dari Button
     */
    public void flipCard(MatchPairCard selectedCard) {
        if (selectedCard1 == null && selectedCard2 == null) {
            selectedCard1 = selectedCard;
            selectedCard1.show();
        }
        if (selectedCard1 != null && selectedCard1 != selectedCard && selectedCard2 == null) {
            selectedCard2 = selectedCard;
            selectedCard2.show();
            checkCardsTimer.start();
        }
    }

    /**
     * Method untuk mengecek apakah kedua kartu mempunyai
     * gambar yang sama atau tidak.
     */
    public void checkCards() {
        if (selectedCard1.getId() == selectedCard2.getId()) {
            selectedCard1.getButton().setEnabled(false);
            selectedCard2.getButton().setEnabled(false);
            selectedCard1.setMatched(true);
            selectedCard2.setMatched(true);
            if (isWon()) {
                Object[] choices = {" Quit", " Play Again!"};
                int option = JOptionPane.showOptionDialog(null,
                        null,
                        "You Won!", JOptionPane.YES_NO_CANCEL_OPTION,
                        JOptionPane.PLAIN_MESSAGE, null, choices, choices[1]);
                if (option == 1) {
                    resetGame(cardList);
                } else {
                    System.exit(0);
                }
            }
        } else {
            selectedCard1.hide();
            selectedCard2.hide();
            tryAttempts++;
            tryLabel.setText("Number of Tries: " + tryAttempts);
        }
        selectedCard1 = null;
        selectedCard2 = null;
    }

    /**
     * Method yang mengecek apakah user sudah memenangkan Memory Game dengan mengecek
     * semua kartu yang ada di MatchPairBoard sudah mempunyai pasangannya masing - masing.
     *
     * @return boolean true jika semua kartu sudah ada pasangannya
     * false jika ada kartu yang belum mempunyai pasangan
     */
    public boolean isWon() {
        for (MatchPairCard card : cardList) {
            if (!card.isMatched()) {
                return false;
            }
        }
        return true;
    }

    /**
     * Method untuk menampilkan semua kartu yang ada di MatchPairBoard.
     */
    public void showAll() {
        for (MatchPairCard card : cardList) {
            card.show();
        }
        Timer hideAllTimer = new Timer(2000, act -> hideAll());
        hideAllTimer.setRepeats(false);
        hideAllTimer.start();
    }

    /**
     * Method untuk menyembunyikan semua kartu yang ada di MatchPairBoard.
     */
    public void hideAll() {
        for (MatchPairCard card : cardList) {
            card.hide();
        }
    }

    /**
     * Method untuk mengulang permainan dari awal.
     *
     * @param cardList List yang ada di objek MatchPairBoard
     */
    public void resetGame(ArrayList<MatchPairCard> cardList) {
        frame.remove(topPanel);
        Collections.shuffle(cardList);
        topPanel = new JPanel(new GridLayout(6, 6));
        for (MatchPairCard card : cardList) {
            card.setMatched(false);
            card.hide();
            card.getButton().setEnabled(true);
            topPanel.add(card.getButton());
        }
        frame.add(topPanel, BorderLayout.CENTER);
        resetAttempts++;
        tryAttempts = 0;
        resetLabel.setText("Number of Reset: " + resetAttempts);
        tryLabel.setText("Number of Tries: " + tryAttempts);
        showAll();
    }

}
