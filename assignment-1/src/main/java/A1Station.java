import java.util.Scanner;

public class A1Station {

    // Variabel static dari class TrainCar
    private static final double THRESHOLD = 250; // in kilograms
    private static String[] inputs;
    private static String name = "";
    private static double weight = 0;
    private static double length = 0;
    private static double average;
    private static String category = "";
    private static WildCat cat = new WildCat(name, weight, length);
    private static TrainCar trainCar = new TrainCar(cat);
    private static int i;

    // Fungsi static untuk print output sesuai format soal
    public static void printTrain() {
        System.out.print("The train departs to Javari Park\n"
                + "[LOCO]<--");
        trainCar.printCar();
        average = trainCar.computeTotalMassIndex() / ((double) i);
        if (average < 18.5) {
            category = "underweight";
        } else if (average >= 18.5 && average < 25) {
            category = "normal";
        } else if (average >= 25 && average < 30) {
            category = "overweight";
        } else if (average >= 30) {
            category = "obese";
        }
        System.out.printf("Average mass index of all cats: %.2f\n"
                        + "In average, the cats in the train are *%s*\n",
                average, category);
    }

    // Method main dari class A1Station
    public static void main(String[] args) {
        // Declare variabel untuk menerima input dari user
        Scanner in = new Scanner(System.in);

        // Banyak kucing yang akan di assign
        int n = Integer.parseInt(in.nextLine());
        i = 1;

        while (i <= n) {
            // Memisahkan input dari user sesuai format soal
            inputs = in.nextLine().split(",");
            name = inputs[0];
            weight = Double.parseDouble(inputs[1]);
            length = Double.parseDouble(inputs[2]);

            // Membuat objek WildCat baru
            cat = new WildCat(name, weight, length);

            // Jika belum ada TrainCar yang di assign ke train
            // maka TrainCar tersebut paling ujung di train
            if (i == 1) {
                trainCar = new TrainCar(cat);
                // Jika sudah ada TrainCar di train
            } else {
                trainCar = new TrainCar(cat, trainCar);
            }

            // Jika berat total dari train melebihi batas
            // train akan di depart dan menambah train baru
            if (trainCar.computeTotalWeight() > 250) {
                printTrain();
                n -= i;
                i = 0;
                // Jika traincar sudah sampai di objek terakhir
            } else if (i == n) {
                printTrain();
            }
            i++;
        }
    }
}

