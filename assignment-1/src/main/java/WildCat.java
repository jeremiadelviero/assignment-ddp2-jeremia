import java.lang.Math;

public class WildCat {
    // Variabel dari class WildCat untuk masing masing instance
    public String name;
    public double weight; // In kilograms
    public double length; // In centimeters

    // Constructor untuk objek WildCat
    public WildCat(String name, double weight, double length) {
        // Assign parameter ke variabel objek
        this.name = name;
        this.weight = weight;
        this.length = length;
    }

    public double computeMassIndex() {
        // Menghitung Body Mass Index (BMI)
        return this.weight / Math.pow((this.length / 100), 2);
    }
}
