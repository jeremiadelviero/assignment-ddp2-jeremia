public class TrainCar {
    // Variabel static dari class TrainCar
    public static final double EMPTY_WEIGHT = 20; // In kilograms

    // Variabel dari class TrainCar untuk masing masing instance
    public WildCat cat;
    public TrainCar next;
    public boolean lastCar = false;

    // Constructor untuk objek TrainCar
    public TrainCar(WildCat cat) {
        // Assign parameter ke variabel objek
        this.cat = cat;
        this.lastCar = true;
    }

    // Constructor overload untuk objek TrainCar
    public TrainCar(WildCat cat, TrainCar next) {
        // Assign parameter ke variabel objek
        this.cat = cat;
        this.next = next;
    }

    // Fungsi rekursif untuk menghitung berat semua kereta + kucing
    public double computeTotalWeight() {
        // Base Case
        if (this.lastCar) {
            return this.EMPTY_WEIGHT + this.cat.weight;
            // Recursive Case
        } else {
            return this.EMPTY_WEIGHT + this.cat.weight + this.next.computeTotalWeight();
        }
    }

    // Fungsi rekursif untuk menghitung berat Body Mass Index semua kucing
    public double computeTotalMassIndex() {
        // Base Case
        if (this.lastCar) {
            return this.cat.computeMassIndex();
            // Recursive Case
        } else {
            return this.cat.computeMassIndex() + this.next.computeTotalMassIndex();
        }
    }

    // Fungsi rekursif untuk print susunan kereta
    public void printCar() {
        // Base Case
        if (this.lastCar) {
            System.out.printf("(%s)\n", this.cat.name);
            // Recursive Case
        } else {
            System.out.printf("(%s)--", this.cat.name);
            this.next.printCar();
        }
    }
}
