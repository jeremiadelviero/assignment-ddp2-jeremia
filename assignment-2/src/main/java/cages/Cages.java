// Memasukkan kelas ke dalam package cages

package cages;

// Import kelas Animals dari package animals

import animals.Animals;

import java.util.ArrayList;

// Import kelas ArrayList untuk membuat arraylist

public class Cages {
    // Instance Variable
    private ArrayList<Animals> animalsArrayList = new ArrayList<Animals>();
    private Animals[][] levels = new Animals[3][];

    public Cages(ArrayList<Animals> animalsArrayList) {
        this.animalsArrayList = animalsArrayList;
    }

    public void arrange() {
        System.out.printf("location: %s\n", this.animalsArrayList.get(0).getLocationCages());
        ArrayList<Animals> temp1 = new ArrayList<Animals>();
        ArrayList<Animals> temp2 = new ArrayList<Animals>();
        ArrayList<Animals> temp3 = new ArrayList<Animals>();
        if (this.animalsArrayList.size() > 2) {
            for (int i = 0; i < this.animalsArrayList.size(); i++) {
                if (i < this.animalsArrayList.size() / 3) {
                    temp1.add(this.animalsArrayList.get(i));
                    this.levels[0] = temp1.toArray(new Animals[temp1.size()]);
                } else if (i >= this.animalsArrayList.size() / 3
                        && i < this.animalsArrayList.size() * 2 / 3) {
                    temp2.add(this.animalsArrayList.get(i));
                    this.levels[1] = temp2.toArray(new Animals[temp2.size()]);
                } else {
                    temp3.add(this.animalsArrayList.get(i));
                    this.levels[2] = temp3.toArray(new Animals[temp3.size()]);
                }
            }
        } else {
            for (int i = 0; i < this.animalsArrayList.size(); i++) {
                if (i == 0) {
                    temp1.add(this.animalsArrayList.get(i));
                    this.levels[0] = temp1.toArray(new Animals[temp1.size()]);
                } else if (i == 1) {
                    temp2.add(this.animalsArrayList.get(i));
                    this.levels[1] = temp2.toArray(new Animals[temp2.size()]);
                }
            }
        }
        for (int j = 2; j >= 0; j--) {
            System.out.printf("level %d: ", j + 1);
            if (!(this.levels[j] == null)) {
                for (int k = 0; k < this.levels[j].length; k++) {
                    System.out.printf("%s (%d - %s), ", this.levels[j][k].getName(),
                            this.levels[j][k].getBodyLength(),
                            this.levels[j][k].getTypeCages());
                }
            }
            System.out.println();
        }
    }

    public void rearrange() {
        for (int i = 0; i < this.levels.length; i++) {
            if (!(this.levels[i] == null)) {
                for (int j = 0; j < this.levels[i].length / 2; j++) {
                    Animals temp = this.levels[i][j];
                    this.levels[i][j] = this.levels[i][this.levels[i].length - j - 1];
                    this.levels[i][this.levels[i].length - j - 1] = temp;
                }
            }
        }

        if (this.animalsArrayList.size() > 2) {
            Animals[][] temp = new Animals[3][this.levels[2].length];
            for (int i = 0; i < this.levels.length; i++) {
                if (i + 1 == this.levels.length) {
                    temp[0] = this.levels[i];
                } else {
                    temp[i + 1] = this.levels[i];
                }
            }
            this.levels = temp;
        } else {
            Animals[][] temp = new Animals[3][this.levels[0].length];
            for (int i = 0; i < this.levels.length; i++) {
                if (i + 1 == this.levels.length) {
                    temp[0] = this.levels[i];
                } else {
                    temp[i + 1] = this.levels[i];
                }
            }
            this.levels = temp;
        }
        for (int j = 2; j >= 0; j--) {
            System.out.printf("level %d: ", j + 1);
            if (!(this.levels[j] == null)) {
                for (int k = 0; k < this.levels[j].length; k++) {
                    System.out.printf("%s (%d - %s), ", this.levels[j][k].getName(),
                            this.levels[j][k].getBodyLength(),
                            this.levels[j][k].getTypeCages());
                }
            }
            System.out.println();
        }
    }

    public ArrayList<Animals> getAnimalsArrayList() {
        return animalsArrayList;
    }

    public void setAnimalsArrayList(ArrayList<Animals> animalsArrayList) {
        this.animalsArrayList = animalsArrayList;
    }

    public Animals[][] getLevels() {
        return levels;
    }

    public void setLevels(Animals[][] levels) {
        this.levels = levels;
    }

}