// Mengimport package yang dibutuhkan

import animals.Animals;
import animals.Cat;
import animals.Eagle;
import animals.Hamster;
import animals.Lion;
import animals.Parrot;
import cages.Cages;

import java.util.ArrayList;
import java.util.Scanner;

// Mengimport scanner dan arraylist

public class Assignment2 {
    // Variable static yang digunakan sesuai dengan soal
    private static final String[] animals = {"cat", "lion", "eagle", "parrot", "hamster"};
    private static final String[] animalsVisit = {"cat", "eagle", "hamster", "parrot", "lion"};
    private static Cages[] cages = new Cages[5];

    // Method yang digunakan untuk mencari hewan dalam kandang yang ada
    public static int find(String name, int cage) {
        int counter = 1;
        for (Animals animal : cages[cage].getAnimalsArrayList()) {
            if (animal.getName().equals(name)) {
                System.out.printf("You are visiting %s (%s) now, what would you like to do?",
                        animal.getName(), animal.getAnimalType());
                return counter - 1;
            } else {
                if (counter == cages[cage].getAnimalsArrayList().size()
                        || cages[cage].getAnimalsArrayList() == null) {
                    System.out.printf("There is no %s with that name! ", animals[cage]);
                    return -1;
                }
                counter++;
            }
        }
        return -1;
    }

    // Method yang menampilkan command-command yang tersedia
    public static void commandList(int animalsOrder) {
        switch (animalsOrder) {
            case 0:
                System.out.println("\n1: Brush the fur 2: Cuddle");
                break;
            case 1:
                System.out.println("\n1: See it hunting 2: Brush the mane 3: Disturb it");
                break;
            case 2:
                System.out.println("\n1: Order to fly");
                break;
            case 3:
                System.out.println("\n1: Order to fly 2: Do conversation");
                break;
            case 4:
                System.out.println("\n1: See it gnawing 2: Order to run in the hamster wheel");
                break;
            default:
                break;
        }
    }

    // Method yang menjalankan command dari user dengan
    // melakukan explicit casting untuk setiap hewan
    public static void commandExec(int animalOrder, int order, Animals animal) {
        switch (animalOrder) {
            case 0:
                switch (order) {
                    case 1:
                        ((Cat) animal).brushCat();
                        break;
                    case 2:
                        ((Cat) animal).cuddle();
                        break;
                    default:
                        System.out.println("You do nothing...");
                        break;
                }
                break;
            case 1:
                switch (order) {
                    case 1:
                        ((Lion) animal).hunt();
                        break;
                    case 2:
                        ((Lion) animal).brushLion();
                        break;
                    case 3:
                        ((Lion) animal).disturb();
                        break;
                    default:
                        System.out.println("You do nothing...");
                        break;
                }
                break;
            case 2:
                switch (order) {
                    case 1:
                        ((Eagle) animal).flyEagle();
                        break;
                    default:
                        System.out.println("You do nothing...");
                        break;
                }
                break;
            case 3:
                switch (order) {
                    case 1:
                        ((Parrot) animal).flyParrot();
                        break;
                    case 2:
                        ((Parrot) animal).imitate();
                        break;
                    default:
                        System.out.printf("%s says: HM?\n", animal.getName());
                        break;
                }
                break;
            case 4:
                switch (order) {
                    case 1:
                        ((Hamster) animal).gnaw();
                        break;
                    case 2:
                        ((Hamster) animal).run();
                        break;
                    default:
                        System.out.println("You do nothing...");
                        break;
                }
                break;
            default:
                break;
        }
    }

    // Method main
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Welcome to Javari Park!\nInput the number of animals");
        for (int animal = 0; animal < animals.length; animal++) {
            System.out.print(animals[animal] + ": ");
            int n = in.nextInt();
            String[] infoList = new String[n];
            ArrayList<Animals> temp = new ArrayList<Animals>();
            if (n > 0) {
                System.out.printf("Provide the information of %s(s):\n", animals[animal]);
                infoList = in.next().split(",");
                for (int i = 0; i < infoList.length; i++) {
                    if (infoList[i].split("\\|").length == 2) {
                        switch (animal) {
                            case 0:
                                Animals tempCat = new Cat(infoList[i].split("\\|")[0],
                                        Integer.parseInt(infoList[i].split("\\|")[1]),
                                        animals[animal]);
                                temp.add(i, tempCat);
                                break;
                            case 1:
                                Animals tempLion = new Lion(infoList[i].split("\\|")[0],
                                        Integer.parseInt(infoList[i].split("\\|")[1]),
                                        animals[animal]);
                                temp.add(i, tempLion);
                                break;
                            case 2:
                                Animals tempEagle = new Eagle(infoList[i].split("\\|")[0],
                                        Integer.parseInt(infoList[i].split("\\|")[1]),
                                        animals[animal]);
                                temp.add(i, tempEagle);
                                break;
                            case 3:
                                Animals tempParrot = new Parrot(infoList[i].split("\\|")[0],
                                        Integer.parseInt(infoList[i].split("\\|")[1]),
                                        animals[animal]);
                                temp.add(i, tempParrot);
                                break;
                            case 4:
                                Animals tempHamster = new Hamster(infoList[i].split("\\|")[0],
                                        Integer.parseInt(infoList[i].split("\\|")[1]),
                                        animals[animal]);
                                temp.add(i, tempHamster);
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
            cages[animal] = new Cages(temp);
        }
        System.out.println("Animals have been successfully recorded!\n");
        System.out.println("=============================================");
        System.out.println("Cage arrangement");
        for (int i = 0; i < cages.length; i++) {
            if (cages[i].getAnimalsArrayList().size() > 0) {
                cages[i].arrange();
                System.out.println("\nAfter rearrangement...");
                cages[i].rearrange();
                System.out.println();
            }
        }
        System.out.println("NUMBER OF ANIMALS:");
        for (int i = 0; i < animals.length; i++) {
            System.out.println(animals[i] + ":" + cages[i].getAnimalsArrayList().size());
        }
        System.out.println("\n=============================================");

        while (true) {
            System.out.println("Which animal you want to visit?\n"
                    + "(1: Cat, 2: Eagle, 3: Hamster, 4: Parrot, 5: Lion, 99: Exit)");
            int visit = in.nextInt();
            int animalOrder = 0;
            int animalFound = 0;
            switch (visit) {
                case 1:
                    animalOrder = 0;
                    break;
                case 2:
                    animalOrder = 2;
                    break;
                case 3:
                    animalOrder = 4;
                    break;
                case 4:
                    animalOrder = 3;
                    break;
                case 5:
                    animalOrder = 1;
                    break;
                default:
                    break;
            }
            int command;
            if (visit == 99) {
                System.exit(0);
            } else if (1 > visit || visit > 5) {
                System.out.println("You do nothing...");
            } else {
                System.out.printf("Mention the name of %s you want to visit: ",
                        animalsVisit[visit - 1]);
                String name = in.next();
                animalFound = find(name, animalOrder);
                if (animalFound != -1) {
                    commandList(animalOrder);
                    command = in.nextInt();
                    commandExec(animalOrder, command,
                            cages[animalOrder].getAnimalsArrayList().get(animalFound));

                }
            }
            System.out.println("Back to the office!\n");
        }
    }
}

