// Memasukkan kelas ke dalam package animals

package animals;

public class Lion extends Animals {

    // Constructor yang memanggil constructor superclass nya
    public Lion(String name, int bodyLength, String animalType) {
        super(name, bodyLength, animalType);
    }

    // Method yang dimiliki masing - masing hewan
    public void hunt() {
        System.out.printf("Lion is hunting..\n%s makes a voice: Err....\n", this.getName());
    }

    public void brushLion() {
        System.out.printf("Clean the lion's mane..\n%s makes a voice: Hauhhmm!\n", this.getName());
    }

    public void disturb() {
        System.out.printf("%s makes a voice: HAAHUM!!\n", this.getName());
    }
}