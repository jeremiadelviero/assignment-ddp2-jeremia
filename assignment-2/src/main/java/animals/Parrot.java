// Memasukkan kelas ke dalam package animals

package animals;

// Import kelas Scanner untuk meminta input dari user

import java.util.Scanner;

public class Parrot extends Animals {

    // Constructor yang memanggil constructor superclass nya
    public Parrot(String name, int bodyLength, String animalType) {
        super(name, bodyLength, animalType);
    }

    // Method yang dimiliki masing - masing hewan
    public void flyParrot() {
        System.out.printf("Parrot %s flies!\n%s makes a voice: TERBAAANG...\n",
            this.getName(), this.getName());
    }

    public void imitate() {
        Scanner in = new Scanner(System.in);
        System.out.print("You say: ");
        String speech = in.nextLine();
        if (!speech.equals("") && !(speech == null)) {
            System.out.printf("%s says: %s\n", this.getName(), speech.toUpperCase());
        } else {
            System.out.printf("%s says: HM?\n", this.getName());
        }
    }
}