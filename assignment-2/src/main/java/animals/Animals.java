// Memasukkan kelas ke dalam package animals

package animals;

public class Animals {
    // Instance Variable
    private String name;
    private String animalType;
    private int bodyLength;
    private String locationCages;
    private String typeCages;

    // Constructor kelas Animals
    public Animals(String name, int bodyLength, String animalType) {
        this.name = name;
        this.bodyLength = bodyLength;
        this.animalType = animalType;
        // Menempatkan hewan peliharaan di dalam ruangan
        if (this.animalType.equals("cat") || this.animalType.equals("hamster")
                || this.animalType.equals("parrot")) {
            this.locationCages = "indoor";
            // Menempatkan hewan liar di luar ruangan
        } else if (this.animalType.equals("lion") || this.animalType.equals("eagle")) {
            this.locationCages = "outdoor";
        }
        // Menentukan ukuran kandang berdasarkan panjang hewan dan lokasi kandang
        if (locationCages.equals("indoor")) {
            if (bodyLength < 45) {
                this.typeCages = "A";
            } else if (bodyLength >= 45 && bodyLength <= 60) {
                this.typeCages = "B";
            } else {
                this.typeCages = "C";
            }
        } else if (locationCages.equals("outdoor")) {
            if (bodyLength < 75) {
                this.typeCages = "A";
            } else if (bodyLength >= 75 && bodyLength <= 90) {
                this.typeCages = "B";
            } else {
                this.typeCages = "C";
            }
        }
    }

    // Setter dan Getter
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAnimalType() {
        return animalType;
    }

    public void setAnimalType(String animalType) {
        this.animalType = animalType;
    }

    public int getBodyLength() {
        return bodyLength;
    }

    public void setBodyLength(int bodyLength) {
        this.bodyLength = bodyLength;
    }

    public String getLocationCages() {
        return locationCages;
    }

    public void setLocationCages(String locationCages) {
        this.locationCages = locationCages;
    }

    public String getTypeCages() {
        return typeCages;
    }

    public void setTypeCages(String typeCages) {
        this.typeCages = typeCages;
    }
}








