// Memasukkan kelas ke dalam package animals

package animals;

public class Eagle extends Animals {

    // Constructor yang memanggil constructor superclass nya
    public Eagle(String name, int bodyLength, String animalType) {
        super(name, bodyLength, animalType);
    }

    // Method yang dimiliki masing - masing hewan
    public void flyEagle() {
        System.out.printf("%s makes a voice: Kwaakk....\nYou hurt!\n", this.getName());
    }
}