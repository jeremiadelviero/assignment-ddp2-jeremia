// Memasukkan kelas ke dalam package animals

package animals;

public class Hamster extends Animals {

    // Constructor yang memanggil constructor superclass nya
    public Hamster(String name, int bodyLength, String animalType) {
        super(name, bodyLength, animalType);
    }

    // Method yang dimiliki masing - masing hewan
    public void gnaw() {
        System.out.printf("%s makes a voice: Ngkkrit.. Ngkkrrriiit\n", this.getName());
    }

    public void run() {
        System.out.printf("%s makes a voice: Trrr... Trrr...\n", this.getName());
    }
}

