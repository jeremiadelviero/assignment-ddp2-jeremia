// Memasukkan kelas ke dalam package animals

package animals;

// Import kelas Random untuk memproduksi angka random

import java.util.Random;

public class Cat extends Animals {
    // List yang dipakai ketika method cuddle() dijalankan
    private static final String[] cuddleSounds = {"Miaaaw..", "Purrr..",
        "Mwaw!", "Mraaawr!"};

    // Constructor yang memanggil constructor superclass nya
    public Cat(String name, int bodyLength, String animalType) {
        super(name, bodyLength, animalType);
    }

    // Method yang dimiliki masing - masing hewan
    public void brushCat() {
        System.out.printf(("Time to clean %s's fur\n"
                + "%s makes a voice: Nyaaan...\n"), this.getName(), this.getName());
    }

    public void cuddle() {
        Random rand = new Random();
        int sound = rand.nextInt(4);
        System.out.printf(("%s makes a voice: %s\n"), this.getName(), this.cuddleSounds[sound]);
    }
}